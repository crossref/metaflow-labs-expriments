import math
import concurrent.futures
from tenacity import retry, stop_after_attempt, wait_random_exponential
import logging
import requests
import urllib.parse as urlparse
from urllib.parse import urlencode
from time import sleep

SAMPLER_USER_AGENT = {"User-Agent": "Crossref Sampler; (mailto:gbilder@crossref.org)"}

MAX_WORKERS = 2 
MAX_SINGLE_SAMPLE_SIZE = 100 # This is hard limit in the API


DEBUG_MAX_SAMPLE_SIZE = 100

CONFIDENCE_LEVEL = 95.0
CONFIDENCE_INTERVAL = 0.5

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)


# CALCULATE THE SAMPLE SIZE
# Script from http://veekaybee.github.io/how-big-of-a-sample-size-do-you-need/
# on how to calculate sample size, adjusted for my own population size
# and confidence intervals
# Original here: http://bc-forensics.com/?p=15
# Supported confidence levels: 50%, 68%, 90%, 95%, and 99%
confidence_level_constant = [50, 0.67], [68, 0.99], [90, 1.64], [95, 1.96], [99, 2.57]


def calculate_sample_size(
    population_size,
    confidence_level=CONFIDENCE_LEVEL,
    confidence_interval=CONFIDENCE_INTERVAL,
):
    """
    return a needed sample size
    """
    Z = 0.0
    p = 0.5
    e = confidence_interval / 100.0
    N = population_size
    n_0 = 0.0
    n = 0.0

    # Loop through supported confidence levels and find the num std
    # deviations for that confidence level
    for i in confidence_level_constant:
        if i[0] == confidence_level:
            Z = i[1]

    if Z == 0.0:
        return -1

    # Calc sample size
    n_0 = ((Z ** 2) * p * (1 - p)) / (e ** 2)

    # Adjust sample size for finite population
    n = n_0 / (1 + ((n_0 - 1) / float(N)))

    return int(math.ceil(n))  # THE SAMPLE SIZE


def add_sample_param(url, sample_size=10):
    """
    takes a URL and adds (or, if it already exists, replaces) the sample param
    and returns the URL with the new parameter
    """
    params = {"sample": sample_size}
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def generate_sample_args(
    url,
    size=0,
):
    """
    return a list of request URLs that can be executed
    in parallel in order to  generate complete sample
    """
    sample_count = math.ceil(size / MAX_SINGLE_SAMPLE_SIZE)
    return [add_sample_param(url, MAX_SINGLE_SAMPLE_SIZE)] * sample_count


@retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=1, max=60))
def crapi(url, headers={}):
    """
    quick & dirty Crossref API request with retrying and exponential backoff
    """
    logger.debug(f"executing query...{url} with headers {headers}")
    res = requests.get(url, headers=headers)
    if res.status_code == 200:
        return res.json()["message"]

    logger.error(f"request error: {res.status_code}")

    return {"items": []}


def get_all_results(queries, headers={}):
    """
    given a list of request URIs, execute them in parallel using futures
    """

    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        future_to_url = {
            executor.submit(crapi, query, headers): query for query in queries
        }

        items = []
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
                items += data["items"]
            except Exception as exc:
                logger.error("%r generated an exception: %s" % (url, exc))

    return items


def get_population(url):
    """
    total-results for whatever request is given
    """
    try:
        return requests.get(url).json()["message"]["total-results"]
    except Exception as e:
        logger.error(f"Failed to get poluation size: {e}")
        raise e


def get_sample(url):
    population = get_population(url)
    # TODO swap below once sample parameter fixed
    # sample_size = calculate_sample_size(population_size=population)
    sample_size = min(calculate_sample_size(population_size=population), DEBUG_MAX_SAMPLE_SIZE)
    requests = generate_sample_args(url, sample_size)
    all_results = get_all_results(requests, headers=SAMPLER_USER_AGENT)
    return all_results
