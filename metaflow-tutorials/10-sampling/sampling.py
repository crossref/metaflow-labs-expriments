from metaflow import FlowSpec, step, IncludeFile, Parameter


class SamplingFlow(FlowSpec):
    """
    A flow to help you generate a random sample of DOIs.

    The flow performs the following steps:
    1) Fetch a random sample of DOIs from the Crossref REST API.
    2) Stores the DOI, content type, publication year and 
       member name in a CSV file.

    """

    type = Parameter('type',
                      help="Filter DOIs for a particular content type.",
                      default='all')

    member = Parameter('member',
                        help="Filter DOIs for a particular member id.",
                        default='all')

    prefix = Parameter('prefix',
                        help="Filter DOIs for a particular prefix.",
                        default='all')

    funder = Parameter('funder',
                        help="Filter DOIs for a particular funder id.",
                        default='all')

    from_pub_date = Parameter('from_pub_date',
                               help="Filter DOIs published from a particular date.",
                               default='all')

    until_pub_date = Parameter('until_pub_date',
                                help="Filter DOIs published from a particular date.",
                                default='all')

    @step
    def start(self):
        """
        Calculate the expected sample size.

        """
        import math
        from datetime import datetime
        import requests
        import os
        from dotenv import load_dotenv
        load_dotenv();

        CONFIDENCE_LEVEL = 95.0
        CONFIDENCE_INTERVAL = 0.5

        def to_filter_string(filter):
            return ','.join(['{}:{}'.format(k, v) for k, v in filter.items()])

        # CALCULATE THE SAMPLE SIZE
        # Script from http://veekaybee.github.io/how-big-of-a-sample-size-do-you-need/
        # on how to calculate sample size, adjusted for my own population size
        # and confidence intervals
        # Original here: http://bc-forensics.com/?p=15
        # Supported confidence levels: 50%, 68%, 90%, 95%, and 99%
        confidence_level_constant = [50, 0.67], [68, 0.99], [90, 1.64], [95, 1.96], [99, 2.57]

        def calculate_sample_size(
            population_size,
            confidence_level=CONFIDENCE_LEVEL,
            confidence_interval=CONFIDENCE_INTERVAL,
        ):
            """
            return a needed sample size
            """
            Z = 0.0
            p = 0.5
            e = confidence_interval / 100.0
            N = population_size
            n_0 = 0.0
            n = 0.0

            # Loop through supported confidence levels and find the num std
            # deviations for that confidence level
            for i in confidence_level_constant:
                if i[0] == confidence_level:
                    Z = i[1]

            if Z == 0.0:
                return -1

            # Calc sample size
            n_0 = ((Z ** 2) * p * (1 - p)) / (e ** 2)

            # Adjust sample size for finite population
            n = n_0 / (1 + ((n_0 - 1) / float(N)))

            return int(math.ceil(n))  # THE SAMPLE SIZE

        self.fname = f'10-sampling/sample_{str(datetime.now().strftime("%Y-%m-%d-%H:%M:%S"))}.csv'
        
        filter = {}
        if self.type != 'all':
            filter['type'] = self.type
        if self.member != 'all':
            filter['member'] = self.member
        if self.prefix != 'all':
            filter['prefix'] = self.prefix
        if self.funder != 'all':
            filter['funder'] = self.funder
        if self.from_pub_date != 'all':
            filter['from-pub-date'] = self.from_pub_date
        if self.until_pub_date != 'all':
            filter['until-pub-date'] = self.until_pub_date

        url = 'https://api.crossref.org/works'
        query = {'mailto': os.environ.get("CR_API_MAILTO")}
        if filter:
            query['filter'] = to_filter_string(filter)

        # store the total number of DOIs these parameters would return
        self.population_size = requests.get(url, params=query).json().get('message', {}).get('total-results', {})
        
        # calculate sample size
        self.sample_size = calculate_sample_size(population_size=self.population_size)
        
        print(f'Population size: {self.population_size}')
        print(f'Desired sample size: {self.sample_size}')

        # TODO: upper limit for sample size is 1000 until sample API endpoint is more robust
        self.sample_size = min(self.sample_size, 1000)

        self.next(self.fetch)

    @step
    def fetch(self):
        """
        Fetch a random sample from the Crossref REST API.

        """
        from datetime import datetime
        from crossref_commons.sampling import get_sample
        from dotenv import load_dotenv
        load_dotenv();

        self.fname = f'10-sampling/sample_{str(datetime.now().strftime("%Y-%m-%d-%H:%M:%S"))}.csv'
        
        filter = {}
        if self.type != 'all':
            filter['type'] = self.type
        if self.member != 'all':
            filter['member'] = self.member
        if self.prefix != 'all':
            filter['prefix'] = self.prefix
        if self.funder != 'all':
            filter['funder'] = self.funder
        if self.from_pub_date != 'all':
            filter['from-pub-date'] = self.from_pub_date
        if self.until_pub_date != 'all':
            filter['until-pub-date'] = self.until_pub_date
        
        self.json = get_sample(size=self.sample_size, filter=filter)

        self.next(self.parse)

    @step
    def parse(self):
        """
        Parse the API response and store relevant properties in a CSV file.

        """
        import pandas as pd
        import numpy as np
        from datetime import date

        df = pd.json_normalize(self.json)
        ff = pd.DataFrame(df, columns=['DOI','published.date-parts','type','member','publisher'])

        # extract year from date-parts
        # return 9999 if year missing
        def get_publication_year(x):
            date_as_string = str(x)
            year = date_as_string[2:6]
            return year if len(year) == 4 else 9999

        ff['published.date-parts'] = ff['published.date-parts'].apply(get_publication_year)
        ff = ff.rename({'published.date-parts': 'year', 'DOI': 'doi'}, axis=1)
        ff.to_csv(self.fname, index=False)

        print(f'{len(ff)} DOIs sampled.')

        self.next(self.end)

    @step
    def end(self):
        """
        Print out some basic statistics about the random DOIs collected.

        """

if __name__ == '__main__':
    SamplingFlow()
